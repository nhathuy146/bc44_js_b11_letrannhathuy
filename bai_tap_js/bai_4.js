//input: chiều dài, chiều rộng
var chieuDai = 5;
var chieuRong = 9;

// Xử lý
const dienTich = chieuDai * chieuRong;
const chuVi = (chieuDai + chieuRong) * 2;

//Output: diện tích, chu vi
console.log(`Chu vi hình chữ nhật: `, chuVi);
console.log(`Diện tích hình chữ nhật: `, dienTich);
