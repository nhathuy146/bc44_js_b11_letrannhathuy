//input: 1 số có 2 chữ số
var number = 76;

// Xử lý
var soHangChuc = Math.floor(number / 10);
var soDonVi = number % 10;
var tongHaiKySo = soHangChuc + soDonVi;

//ouput: Tổng 2 ký số
console.log(`Tổng hai ký số: `, tongHaiKySo);
